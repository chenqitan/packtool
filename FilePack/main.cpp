//
//  main.cpp
//  FilePack
//
//  Created by chenqitan on 15-6-1.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//


#include "BuildPackage.h"
#include "PackageInterface.h"

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <ctime>
#include <cstdlib>

#include "TestAttribute.h"
#include "SysAPITest.h"
#include "PackAPITest.h"


int main(int argc, const char * argv[])
{
  
/************************************************************************************************
     
     1. 打包
     
************************************************************************************************/
     //*
    bool bpack;
    
    std::string OutputPos="";
    std::string BasePath="";
    std::string InputPath="";
    
    std::cout<<"<0=结束输入或者否定含义>"<<std::endl<<"[是否打包]:"<<std::endl;
    std::cin>>bpack;
    
    if(bpack==1)
    {
    BuildPackage    bk;      

    std::vector<std::string> InputPath_Array;
    bool Typing=true;
    if(Typing)
    {
        std::cout<<"[资源包输出位置]:"<<std::endl;
        std::cin>>OutputPos;
        std::cout<<"[以哪个路径存储相对名名称]:"<<std::endl;
        std::cin>>BasePath;
        std::cout<<"[需要打包的目录]:"<<std::endl;
        while(InputPath!="0")
        {
            std::cin>>InputPath;
            if(InputPath!="0")
            InputPath_Array.push_back(InputPath);
        }   
    }
    else
    {
        OutputPos="/Users/chenqitan/Desktop/FilePackProject/GameSource/视频/abc.cab";
        BasePath="/Users/chenqitan/Desktop/FilePackProject/GameSource/";
        InputPath="/Users/chenqitan/Desktop/FilePackProject/GameSource/";
        InputPath_Array.push_back(InputPath);
    }
    bk.BuildingPackage(InputPath_Array,BasePath,OutputPos);
    }
    //*/
/************************************************************************************************
     
     2. 提供访问API
     
************************************************************************************************/
    /*
    PackageInterface pkAPI;
    std::string BaseFileName="/Users/chenqitan/Desktop/FilePackProject/GameSource/";
    pkAPI.LoadCAB("/Users/chenqitan/Desktop/FilePackProject/GameSource/abc.cab",BaseFileName);

     
    unsigned char* buffer=NULL;
    buffer = new unsigned char[1000];
    struct FileInfo* sfi;
    
     //
    sfi=pkAPI.OpenFile(GettingFileName0);
    pkAPI.ReadFile(buffer,1,1,sfi);
    pkAPI.CloseFile(sfi);
    
    std::string s((char*)buffer);
    //std::cout<<std::endl<<"Buffer:"<<s<<std::endl<<std::endl<<std::endl;
    
     
     //*/
     
/************************************************************************************************
     
     3. 效率对比测试
     
************************************************************************************************/
    
    
/**************
     
  输入资源包
     
**************/
    std::vector<std::string> vec_cab;
    std::cout<<"输入资源包："<<std::endl;
    //vec_cab.push_back(OutputPos);
    std::string cabpos="";
    while(1)
    {
        
        std::cin>>cabpos;
        if(cabpos=="0") break;
        else {vec_cab.push_back(cabpos);}
    }
    std::cout<<"-------------------------------------"<<std::endl;
    PackageInterface* _pk=new PackageInterface();
    std::vector<std::string>::iterator _it_cab;
    for(_it_cab=vec_cab.begin();_it_cab<vec_cab.end();_it_cab++)
    {
        _pk->LoadCAB((*_it_cab),"");
    }

    
/**************
     
  随机配置信息
     
**************/
    int toatal_cnt=(int)(_pk->m_Count);
    srand(time(NULL));
    int rnd_file;
    int rnd_cnt;
    int allcounter=0;
    std::string file_name;
    
    int THREAD_COUNT;
    std::cout<<"输入多线程数量："<<std::endl;
    std::cin>>THREAD_COUNT;
       
    TestAttribute* argss[THREAD_COUNT];
    TestAttribute* argss_duplicate[THREAD_COUNT];
    
    for(int i=0;i<THREAD_COUNT;i++)
    {
        argss[i]=new TestAttribute();
    }
    for(int i=0;i<THREAD_COUNT;i++)
    {
        int thcouter=0;
        for (int j=0;j<toatal_cnt; j++)
        {
            rnd_file=rand()%toatal_cnt;
            rnd_cnt=rand()%2+1;
            file_name=_pk->m_FileArray[rnd_file];
            argss[i]->AddFileCnt(file_name,rnd_cnt,i);
            std::cout<<"线程"<<i<<": "<<rnd_cnt<<"次\t"<<PackageInterface::GetFileName(file_name)<<std::endl;
            //argss[i]->AddFileCnt("/Users/chenqitan/Desktop/FilePackProject/GameSource/视频/c.mkv",10,i);
            thcouter+=rnd_cnt;
        }
        std::cout<<"线程"<<i<<": "<<thcouter<<"次"<<std::endl<<std::endl;
        allcounter+=thcouter;
    }
    std::cout<<"总计: "<<""<<allcounter<<"次"<<std::endl;
/**************
 
  性能测试对比
 
**************/
    int preloadnum=0;

    bool save_args=false;
    
    time_t start_time,finish_time;
    double Total_time;
    
    time_t start_clock,finish_clock;
    double Total_clock;
    while(preloadnum!=-1&&THREAD_COUNT!=-1)
    {
        
/**************
         
  文件包API测试
 
**************/
    std::cout<<"输入最大缓存数："<<std::endl;
    std::cin>>preloadnum;
        
    start_time = time(NULL);
    start_clock = clock();
    
    /*
    std::cout<<"*----------------------------------------"<<std::endl;
    std::cout<<"|    文件包API计时-(开始)：time () "<<start_time<<std::endl;
    std::cout<<"|    文件包API计时-(开始)：clock() "<<start_clock<<std::endl;
    std::cout<<"*----------------------------------------"<<std::endl;
    */     
        
    PackAPITest PackTest;     
    PackTest.Timer(argss,preloadnum,THREAD_COUNT,vec_cab);
        
    finish_clock=clock();
    finish_time = time(NULL);
    Total_time = (double)(finish_time-start_time);
    Total_clock = (finish_clock-start_clock);
    /*
    std::cout<<"----------------------------------------"<<std::endl;
    std::cout<<"|    文件包API计时-结束：time() "<<finish_time<<std::endl; 
    std::cout<<"|    文件包API计时-结束： clock() "<<finish_clock<<std::endl;
    */   
    std::cout<<"*----------------------------------------------"<<std::endl;
    std::cout<<"*    文件包API时间-(间隔)：time() "<<Total_time<<std::endl;
    std::cout<<"*    文件包API时间-(间隔)：clock() "<<Total_clock<<std::endl;
    std::cout<<"*----------------------------------------------"<<std::endl<<std::endl<<std::endl;
/**************
         
   系统API测试
         
**************/
           
    start_time = time(NULL);
    start_clock = clock();
    /*
    std::cout<<"*----------------------------------------"<<std::endl;
    std::cout<<"|    系统API计时-开始：time () "<<start_time<<std::endl;
    std::cout<<"|    系统API计时-开始：clock() "<<start_clock<<std::endl;
    std::cout<<"*----------------------------------------"<<std::endl;  
    */
        
    SysAPITest SysTest;
    if(save_args)
        SysTest.Timer(argss_duplicate,THREAD_COUNT);
    else
        SysTest.Timer(argss, THREAD_COUNT);
        
    finish_time = time(NULL);
    finish_clock=clock();
    Total_clock = (finish_clock-start_clock);
    Total_time = (double)(finish_time-start_time);
        
    /*
    std::cout<<"*-------------------------------------"<<std::endl;
    std::cout<<"|    系统API计时-结束：time() "<<finish_time<<std::endl;
    std::cout<<"|    系统API计时-结束：clock() "<<finish_clock<<std::endl;
     */
    std::cout<<"*----------------------------------------------"<<std::endl;
    std::cout<<"*    系统API时间间隔：time() "<<Total_time<<std::endl;
    std::cout<<"*    系统API时间间隔：clock() "<<Total_clock<<std::endl;
    std::cout<<"*----------------------------------------------"<<std::endl;
        


        
    }
     //*/
    
/************************************************************************************************
     
     ... 结束
     
************************************************************************************************/
    std::cout<<"END"<<std::endl;
    pthread_exit( NULL );
    
    return 0;
}

