//
//  SysAPITest.cpp
//  FilePack
//
//  Created by chenqitan on 15-6-6.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//

#include "SysAPITest.h"
#include "TestAttribute.h"

#include <time.h>
#include <pthread.h>

void* SysAPITest::Process(void* args)
{
    TestAttribute*p=(TestAttribute*) args;
    
    //std::cout<<"Loading- "<<p->m_CurrentID<<std::endl;
    
    std::vector<struct TestFileCount>::iterator it_vec;
    for(it_vec=p->m_TestAttrArray.begin();it_vec<p->m_TestAttrArray.end();it_vec++)
    {
        //文件调用次数
        for (int i=0; i<(*it_vec).Count; i++)
        {
            unsigned char* tbuffer=NULL;
            FILE* pWork=NULL;
            
            //打开文件
            pWork = fopen((*it_vec).TestFileName.c_str(),"rb");
            if(pWork!=NULL)
            {
                //获取长度
                fseek(pWork, 0, SEEK_END);
                long lsize=ftell(pWork);
                tbuffer=new unsigned char[lsize];
                //std::cout<<"读取长度： "<<lsize<<std::endl;

                fseek(pWork,0,SEEK_SET);
                fread(tbuffer,lsize,1,pWork);
            }
            
            
            //关闭文件
            fclose(pWork);  
            delete [] tbuffer;
        }
    }
}
void SysAPITest::Timer(TestAttribute* args[],int ThreadNum)
{
    int THREAD_COUNT=ThreadNum; 
    pthread_t ptr[THREAD_COUNT];
    for( int i = 0; i < THREAD_COUNT; ++i )
    {
        //argss[i]->m_TestAttrArray=i;
        int ret = pthread_create( &ptr[i],NULL, Process, args[i] ); //参数：创建的线程id，线程参数，线程运行函数的起始地址，运行函数的参数
        if( ret != 0 ) //创建线程成功返回0
        {
            std::cout << "pthread_create error:error_code=" << ret << std::endl;
        }
        
    }
    for( int i = 0; i < THREAD_COUNT; ++i )
        pthread_join( ptr[i], NULL ); //pthread_join用来等待一个线程的结束，是一个线程阻塞的函数
}