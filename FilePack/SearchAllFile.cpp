//
//  SearchAllFile.cpp
//  FilePack
//
//  Created by chenqitan on 15-6-1.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//

#include "SearchAllFile.h"
#include <iostream>
#include <fstream>
#include <sys/dir.h>
#include <sys/stat.h>


bool SearchFile::isDir(std::string path)
{
    struct stat st;
    lstat(path.c_str(), &st);
    return 0 != S_ISDIR(st.st_mode);
}

//遍历文件夹de递归函数
void SearchFile::SearchDir(std::string Path, std::string BasePath,int recursive,std::vector<std::string>& Vec_AbsoluteFile,std::vector<std::string>& Vec_RelativeFile)
{
    DIR *pdir;
    struct dirent *pdirent;
    
    char temp[500];
    
    std::string RelativeFile="";
    std::string AbsoluteFile="";
    
    
    try
    {
        pdir = opendir(Path.c_str());
    }
    catch(const char *str)
    {printf("failed open dir");}
    
    if(pdir)
    {
        while((pdirent = readdir(pdir)))
        {
            //跳过"."和".."
            if(strcmp(pdirent->d_name, ".") == 0
               || strcmp(pdirent->d_name, "..") == 0)
                continue;
            AbsoluteFile=Path+"/"+pdirent->d_name;
            RelativeFile=AbsoluteFile.substr(BasePathlength);
            
            sprintf(temp, "%s/%s", Path.c_str(), pdirent->d_name);
            
            //当temp为目录并且recursive为1的时候递归处理子目录
            if(isDir(temp) && recursive)
            {
                SearchDir(temp,BasePath,recursive,Vec_AbsoluteFile,Vec_RelativeFile);
            }
            else
            {
                
                //printf("path:%s\n", pdirent->d_name);
                //std::cout<<"Relative "<<RelativeFile<<std::endl;
                Vec_RelativeFile.push_back(RelativeFile);
                //std::cout<<"Absolute "<<AbsoluteFile<<std::endl;
                Vec_AbsoluteFile.push_back(AbsoluteFile);
            }
        }
    }
    else
    {
        printf("opendir error:%s\n", Path.c_str());
    }
    closedir(pdir);
}


//遍历文件夹的驱动函数
void SearchFile::SearchDir(std::string Path,std::string BasePath,std::vector<std::string>& Vec_AbsoluteFile,std::vector<std::string>& Vec_RelativeFile)
{
    InputPathlength=Path.length()-1;
    //去掉最后的‘/’
    if(BasePath[BasePath.length()-1]=='/') BasePath=BasePath.substr(0,BasePath.length()-1);
    BasePathlength=BasePath.length();
    
    unsigned long len;
    char temp[256];
    //去掉末尾的'/'
    len = strlen(Path.c_str());
    strcpy(temp, Path.c_str());
    if(temp[len - 1] == '/') temp[len -1] = '\0';
    if(BasePath[BasePath.length()-1]=='/') BasePath=BasePath.substr(0,BasePath.length()-1);
    
    if(isDir(temp))
    {
        //处理目录
        int recursive = 1;
        SearchDir(temp,BasePath,recursive,Vec_AbsoluteFile,Vec_RelativeFile);
    }
    else   //输出文件
    {
        //printf("path:%s\n", path);
        Vec_AbsoluteFile.push_back(Path);
        //int pos = Path.find_last_of('/');
        Vec_RelativeFile.push_back(Path.substr(BasePathlength));
        //std::cout<<Path.substr(BasePathlength)<<std::endl;
    }
}
