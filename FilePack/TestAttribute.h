//
//  TestAttribute.h
//  FilePack
//
//  Created by chenqitan on 15-6-6.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//

#ifndef __FilePack__TestAttribute__
#define __FilePack__TestAttribute__

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "PackageInterface.h"


//记录某个文件测试信息的结构体
struct TestFileCount
{
    std::string TestFileName;
    int Count;
    int ThreadID;
};
//添加测试信息
class TestAttribute
{
private:
    struct TestFileCount TFC;
public:
    //std::map<std::string,int> m_TestAttr;
    std::vector<struct TestFileCount> m_TestAttrArray;
    void AddFileCnt(std::string TargetFileName,int Cnt,int ThreadID);
    int m_CurrentID;
    std::string m_CurrentCAB;
    PackageInterface* m_pkAPI;
};

#endif /* defined(__FilePack__TestAttribute__) */
