//
//  PackAPITest.h
//  FilePack
//
//  Created by chenqitan on 15-6-6.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//

#ifndef __FilePack__PackAPITest__
#define __FilePack__PackAPITest__

#include <iostream>
#include "TestAttribute.h"
#include "PackageInterface.h"


class PackAPITest
{

private:
    static void* Process(void* args);
public:
    void Timer(TestAttribute* args[],int PreLoadNum,int ThreadNum,std::vector<std::string>& CabPath);
    
};





#endif /* defined(__FilePack__PackAPITest__) */
