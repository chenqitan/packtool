//
//  SysAPITest.h
//  FilePack
//
//  Created by chenqitan on 15-6-6.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//

#ifndef __FilePack__SysAPITest__
#define __FilePack__SysAPITest__

#include <iostream>
#include "TestAttribute.h"

class SysAPITest
{
private:
    static void* Process(void* args);
public:
    void Timer(TestAttribute* args[],int ThreadNum);
};

#endif /* defined(__FilePack__SysAPITest__) */
