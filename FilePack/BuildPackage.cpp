//
//  Packing.cpp
//  FilePack
//
//  Created by chenqitan on 15-6-1.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//

#include "BuildPackage.h"
#include "SearchAllFile.h"
#include <errno.h>
#include <iostream>
#include <stdio.h>
#include <fstream>

BuildPackage::BuildPackage()
{
};

long BuildPackage::GetFileSize(FILE *pf)
{
    fseek(pf,0,SEEK_END);
    return ftell(pf);
}
/*
void BuildPackage::SetOutputFile(std::string OutputFile )
{
    m_OutPutFile=OutputFile;
}

void BuildPackage::AddInputFile(std::string InputFile)
{
    m_FileArray.push_back(InputFile);
}
*/
void BuildPackage::BuildingPackage(std::vector<std::string> Vec_path,std::string BasePath,std::string OutputPos)
{
    std::cout<<"打包开始"<<std::endl;
    std::vector<std::string>::iterator abosoluteFileArray_it;
    std::vector<std::string>::iterator relativeFileArray_it;
    std::vector<long>::iterator fileLength_it;
    
    std::ofstream PackFile(OutputPos.c_str(),std::ios::out);
    std::ifstream SonFile;
    
    std::vector<std::string>::iterator vecPath_it;
    SearchFile sf;
    for(vecPath_it=Vec_path.begin();vecPath_it<Vec_path.end();vecPath_it++)
    {
        sf.SearchDir(*vecPath_it,BasePath,m_AbosoluteFileArray,m_RelativeFileArray);
    }
    /***************
     
    
     1 写入头
     
     
     ***************/
    PackFile<<"                        "<<std::endl;
    PackFile<<m_AbosoluteFileArray.size()<<std::endl;
    for(abosoluteFileArray_it=m_AbosoluteFileArray.begin(),relativeFileArray_it=m_RelativeFileArray.begin();abosoluteFileArray_it<m_AbosoluteFileArray.end();abosoluteFileArray_it++,relativeFileArray_it++)
    {
        //写相对路径
        PackFile<<(*relativeFileArray_it).c_str()<<std::endl;
        SonFile.open((*abosoluteFileArray_it).c_str(),std::ios::binary);
        SonFile.seekg (0, SonFile.end);
        long len=SonFile.tellg();
        m_FileLength.push_back(len);
        //写长度
        PackFile<<len<<std::endl;
        SonFile.close();
    }
    PackFile<<"HEADER-END"<<std::endl;
    
    PackFile.seekp (0, std::ios::end);
    long len=PackFile.tellp();
    //cout<<"1 LENGTH: "<<len<<endl;
    
    PackFile.seekp (0,std::ios::beg);
    PackFile<<len;
    
    PackFile.seekp (0, std::ios::end);
    len=PackFile.tellp();
    //cout<<"2 LENGTH: "<<len<<endl;
    /***************
     
     
     2 写入文件
     
     
     ***************/
    //FILE* pWorkFile;
    char *tmpChar = NULL;
    
    for(abosoluteFileArray_it=m_AbosoluteFileArray.begin(),fileLength_it=m_FileLength.begin();abosoluteFileArray_it<m_AbosoluteFileArray.end();abosoluteFileArray_it++,fileLength_it++)
    {
        //std::cout<<"File Length: "<<*abosoluteFileArray_it<<std::endl;
        //std::cout<<"File Length: "<<*fileLength_it<<std::endl;
        //std::cout<<"File Length: "<<*tmpFileLen<<std::endl;
        
        tmpChar=new char[*fileLength_it];
        SonFile.open((*abosoluteFileArray_it).c_str());
        SonFile.read(tmpChar,*fileLength_it);
        PackFile.write(tmpChar, *fileLength_it);
        delete [] tmpChar;
        SonFile.close();
    }
    PackFile.close();
    
    std::cout<<"打包完成"<<std::endl;

}

