//
//  FileManager.cpp
//  FilePack
//
//  Created by chenqitan on 15-6-3.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//

#include "PackageInterface.h"
#include <errno.h>
#include <fstream>
#include <iostream>
#include <stdio.h>




PackageInterface::PackageInterface()
{
    m_Count=0;
    pthread_mutex_init( &Loading_Mutex, NULL ); //对锁进行初始化
    pthread_mutex_init( &Reading_Mutex, NULL ); //对锁进行初始化

}
void PackageInterface::SetPreLoadNum(int max_load)
{
    MAX_PRLOAD=max_load;
    PreLoad_Vector.clear();
    for(int i=0;i<MAX_PRLOAD;i++)
        PreLoad_Vector.push_back("");
}
PackageInterface::~PackageInterface()
{
    std::vector<FILE*>::iterator file_it;
    std::map<std::string,struct FileInfo>::iterator map_it;
    for(map_it=m_FileMapInfo.begin();map_it!=m_FileMapInfo.end();map_it++)
    {
        if((*map_it).second.Loaded==true)
        {
            delete (*map_it).second.pMemory;
            std::cout<<"释放内存"<<std::endl;
        }
    }
    
}
std::string PackageInterface::GetFileName(std::string PathFileName)
{
    
    return (PathFileName.substr(PathFileName.find_last_of('/')+1));
}

void PackageInterface::Header2Map(std::string CabFile,std::string BasePath)
{
    std::ifstream iStream;
    long HeaderLen=0;
    long Count=0;
    long AddrLen=0;
    long FromAddr=0;
    std::string FileName;
    
    if(BasePath=="")
    {
        BasePath=(CabFile.substr(0,CabFile.find_last_of('/')));
        //std::cout<<BasePath<<std::endl;
    }
    else if(BasePath[BasePath.length()-1]=='/') BasePath=BasePath.substr(0,BasePath.length()-1);
    
    iStream.open(CabFile.c_str());

    iStream>>HeaderLen;

    iStream>>Count;
    m_Count+=Count;
    std::cout<<"载入文件数目：  "<<m_Count<<std::endl;
    
    FromAddr=HeaderLen;
    for(long i=0;i<Count;i++)
    {
        iStream>>FileName;
        FileName=BasePath+FileName;
        
        m_FileArray.push_back(FileName);
        //std::cout<<"FileName:"<<FileName<<std::endl;
        iStream>>AddrLen;
        //std::cout<<"FileLen:"<<AddrLen<<std::endl;
        //
        FI.myName=FileName;
        FI.AddrLength=AddrLen;
        FI.FromAddr=FromAddr;
        FI.CurAddr=0;
        FI.EndAddr=FromAddr+AddrLen-1;
        m_FileMapInfo.insert(std::make_pair(FileName, FI));
        //
        FromAddr+=AddrLen;
    }
    std::cout<<"载入文件大小：  "<<(FromAddr-HeaderLen)/1024/1024<<"MB"<<std::endl<<std::endl;
    //结束标志
    iStream>>FileName;
    //cout<<"Str:"<<FileName<<endl;
    iStream.close();
    
}
void PackageInterface::LoadCAB(std::string CabFile,std::string BasePath)
{
    //互斥锁
    pthread_mutex_lock(&Loading_Mutex );
    pCABA=fopen(CabFile.c_str(),"rb");
    FI.pFile=pCABA;
    Vec_CAB.push_back(pCABA);
    

    //压缩包名
    FI.CabName=CabFile;
    
    
    //std::cout<<CabFile<<FI.pFile<<std::endl;
    if(pCABA==NULL)
    {
        std::cout<<"文件包: "<<GetFileName(CabFile)<<"无法读取"<<std::endl;
    }
    else
    {
        std::cout<<"文件包: "<<GetFileName(CabFile)<<"读取成功"<<std::endl;
        Header2Map(CabFile,BasePath); 
    }
    //解锁
    pthread_mutex_unlock( &Loading_Mutex );
}

bool PackageInterface::HaveFile(std::string TargetFile)
{
    if(m_FileMapInfo[TargetFile].CabName.empty()) return 0;
    else return 1;
}
struct FileInfo* PackageInterface::OpenFile(std::string TargetFile)
{
    //返回信息
    struct FileInfo* mFI=new FileInfo();
    mFI->myName=TargetFile;
    mFI->pFile=m_FileMapInfo[TargetFile].pFile;
    mFI->FromAddr=m_FileMapInfo[TargetFile].FromAddr;
    mFI->EndAddr=m_FileMapInfo[TargetFile].EndAddr;
    mFI->AddrLength=m_FileMapInfo[TargetFile].AddrLength;
    mFI->CabName=m_FileMapInfo[TargetFile].CabName;
    mFI->CurAddr=0;
    mFI->Reading=0;
    if(m_FileMapInfo[TargetFile].Loaded==true)
        mFI->Loaded=true;
    else
        mFI->Loaded=false;
    //
    if(m_FileMapInfo[TargetFile].pMemory==NULL)
        mFI->pMemory=NULL;
    else
        mFI->pMemory=m_FileMapInfo[TargetFile].pMemory;
    //优先级运算
    return mFI;
}

void PackageInterface::PreLoad(std::string MostUsedFile)
{
    

    //pthread_mutex_lock(&PreLoad_Mutex);
    m_FileMapTime[MostUsedFile]++;
    //已经在缓存中：
    if(m_FileMapInfo[MostUsedFile].Loaded==true)
/***
 解锁
 ***/
    {return;}
    
    for(int i=0;i<MAX_PRLOAD;i++)
    {
        //std::cout<<GetFileName(PreLoad_Vector[i])<<"在预加载列表中"<<std::endl;
        if(m_FileMapTime[MostUsedFile]>m_FileMapTime[PreLoad_Vector[i]])
        {
          
/********************************************
 
 删除过程需要与内存读取过程同步
 
 线程1正在读取内存，需要防止线程2删除线程1的读取的内存
 
 
*********************************************/
 
                //std::cout<<"删除缓存: "<<GetFileName(PreLoad_Vector[i])<<std::endl;
            if(m_FileMapInfo[PreLoad_Vector[i]].Reading==0)
            {
                delete [] (char*)m_FileMapInfo[PreLoad_Vector[i]].pMemory;
                m_FileMapInfo[PreLoad_Vector[i]].pMemory=NULL;
                m_FileMapInfo[PreLoad_Vector[i]].Loaded=false;
            }
            else
            {
                return;//因为预加载表实时维护，因此PreLoad_Vector[i]必定是最小值
            }
                //Loading
                //std::cout<<"写入缓存: "<<GetFileName(MostUsedFile)<<std::endl;
            
                unsigned char *pTmpData = NULL;
                pTmpData = new unsigned char[m_FileMapInfo[MostUsedFile].AddrLength];
            
                fseek(m_FileMapInfo[MostUsedFile].pFile,m_FileMapInfo[MostUsedFile].FromAddr,SEEK_SET);
                fread(pTmpData,m_FileMapInfo[MostUsedFile].AddrLength,1,m_FileMapInfo[MostUsedFile].pFile);
            
                m_FileMapInfo[MostUsedFile].pMemory=pTmpData;
                m_FileMapInfo[MostUsedFile].Loaded=true;
                PreLoad_Vector[i]=MostUsedFile;

            break;
        }
        //std::cout<<GetFileName(PreLoad_Vector[i])<<"在预加载列表中"<<std::endl;
    }

    
}
void PackageInterface::ReadFile(void* buffer,size_t size,size_t count,struct FileInfo *sfi)
{

/**************************************
     
 对于读取和写入m_FileMapInfo的操作都要加锁
     
*************************************/
/***
加锁
***/
    pthread_mutex_lock(&Reading_Mutex);
    PreLoad(sfi->myName);
    if(m_FileMapInfo[sfi->myName].Loaded==true)
    {
        
        //std::cout<<pthread_self()<<"从内存中读取: "<<GetFileName(sfi->myName)<<std::endl;
        m_FileMapInfo[sfi->myName].Reading++;
/***
解锁
***/
        pthread_mutex_unlock(&Reading_Mutex);
        memcpy(buffer, (char*)((char*)m_FileMapInfo[sfi->myName].pMemory+sfi->CurAddr),size*count);

/************************************
 
此处可能导致：
 
某线程memcpy刚好执行完而Reading未被减为0的情况下，PreLoad中另一个线程检测Reading为1。
 
这种情况属于可接受范围，因为这样至少不会发生某个内存正在读取突然被其他线程释放内存；而且这种情况发生几率很小。
 
************************************/
        m_FileMapInfo[sfi->myName].Reading--;
        
    }
    else
    {
/**************************************
         
 对于读取和写入m_FileMapInfo的操作都要加锁
         
*************************************/
/***
解锁
***/
        pthread_mutex_unlock(&Reading_Mutex);
        //std::cout<<pthread_self()<<"从磁盘中读取: "<<GetFileName(sfi->myName)<<std::endl;
        fseek(sfi->pFile,sfi->FromAddr+sfi->CurAddr,SEEK_SET);
        fread(buffer,size,count,sfi->pFile);
    }
     
}
void PackageInterface::CloseFile(struct FileInfo* sfi)
{
    delete sfi;
}

void PackageInterface::SeekFile(struct FileInfo* sfi,long offset,int fromwhere)
{
    if(fromwhere==SEEK_SET)
    {
        sfi->CurAddr=offset;
    }
    else if(fromwhere==SEEK_CUR)
    {
        sfi->CurAddr=sfi->CurAddr+offset;
    }
    else if(fromwhere==SEEK_END)
    {
        sfi->CurAddr=sfi->CurAddr-offset;
    }
}

//测试偏移量是否正确
void PackageInterface::GetFileFromPack(std::string TargetName,std::string SavePos)
{
    std::string CabFile=m_FileMapInfo[TargetName].CabName;
    FILE *pWork = NULL;
    FILE *pCAB=m_FileMapInfo[TargetName].pFile;
    unsigned char *pTmpData = NULL;
    pWork = fopen(SavePos.c_str(),"wb");
    
    if(m_FileMapInfo[TargetName].CabName.empty())
    {
        std::cout<<std::endl<<"Not Such File"<<std::endl;
    }
    else
    {
        pTmpData = new unsigned char[m_FileMapInfo[TargetName].AddrLength];
        fseek(pCAB,m_FileMapInfo[TargetName].FromAddr,SEEK_SET);
        fread(pTmpData,m_FileMapInfo[TargetName].AddrLength,1,pCAB);
        fwrite(pTmpData,m_FileMapInfo[TargetName].AddrLength,1,pWork);
        delete [] pTmpData;
        
    }
    fclose(pWork);
}
