//
//  SearchAllFile.h
//  FilePack
//
//  Created by chenqitan on 15-6-1.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//



#ifndef FilePack_SearchAllFile_h
#define FilePack_SearchAllFile_h

#include <vector>
#include <string>


class SearchFile
{
public:SearchFile(){};
    public:
    //遍历文件夹的驱动函数
     void SearchDir(std::string Path,std::string BasePath,std::vector<std::string>& Vec_AbsoluteFile,std::vector<std::string>& Vec_RelativeFile);
    
private:
     static bool isDir(std::string Path);
     void SearchDir(std::string Path, std::string BasePath,int recursive,std::vector<std::string>& Vec_AbsoluteFile,std::vector<std::string>& Vec_RelativeFile);
     int InputPathlength;
     int BasePathlength;
};


#endif
