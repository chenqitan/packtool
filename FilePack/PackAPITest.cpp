//
//  PackAPITest.cpp
//  FilePack
//
//  Created by chenqitan on 15-6-6.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//

#include "PackAPITest.h"
#include "TestAttribute.h"

#include "PackageInterface.h"
#include <pthread.h>
#include <time.h>

PackageInterface* the_pkAPI;

void* PackAPITest::Process(void* args)
{
    TestAttribute*p=(TestAttribute*) args;
    //std::cout<<"Loading Thread "<<p->m_CurrentID<<std::endl;
    std::vector<struct TestFileCount>::iterator it_vec;
      
    for(it_vec=p->m_TestAttrArray.begin();it_vec<p->m_TestAttrArray.end();it_vec++)
    {
        //std::cout<<(*it_vec).ThreadID<<"进入循环 1 "<<std::endl;

        //文件调用次数
        for (int i=0; i<(*it_vec).Count; i++)
        {
            //std::cout<<(*it_vec).ThreadID<<"进入循环 2 "<<std::endl;
            
            unsigned char* tbuffer=NULL;
            struct FileInfo* sfi=NULL;
            
            //打开文件
            sfi=the_pkAPI->OpenFile((*it_vec).TestFileName.c_str());
            tbuffer=new unsigned char[sfi->AddrLength];
            
            //std::cout<<pthread_self()<<"数据长度： "<<sfi->AddrLength<<std::endl;
            //读取数据
            if(the_pkAPI->HaveFile(sfi->myName))
                the_pkAPI->ReadFile(tbuffer,sfi->AddrLength,1,sfi);
            
            //关闭文件
            the_pkAPI->CloseFile(sfi);
            
            //删除buffer
            delete [] tbuffer;
            
        }
        //std::cout<<(*it_vec).ThreadID<<"退出循环 1 "<<std::endl;
    }
    //std::cout<<(*it_vec).ThreadID<<"退出循环 2 "<<std::endl;
}
void PackAPITest::Timer(TestAttribute* args[],int PreLoadNum,int ThreadNum,std::vector<std::string>& CabPath)
{

    the_pkAPI=new PackageInterface();
    int THREAD_COUNT=ThreadNum;
    std::vector<std::string>::iterator it_CabPath;
    for(it_CabPath=CabPath.begin();it_CabPath<CabPath.end();it_CabPath++)
    {
        the_pkAPI->LoadCAB((*it_CabPath),"");
    }
    the_pkAPI->SetPreLoadNum(PreLoadNum);

    pthread_t ptr[THREAD_COUNT];
    
    for( int i = 0; i < THREAD_COUNT; ++i )
    {
        //argss[i]->m_TestAttrArray=i;
        int ret = pthread_create( &ptr[i],NULL, Process, args[i] ); //参数：创建的线程id，线程参数，线程运行函数的起始地址，运行函数的参数
        if( ret != 0 ) //创建线程成功返回0
        {
            std::cout << "pthread_create error:error_code=" << ret << std::endl;
        }
        
    }
    for( int i = 0; i < THREAD_COUNT; ++i )
        pthread_join( ptr[i], NULL ); //pthread_join用来等待一个线程的结束，是一个线程阻塞的函数
    delete the_pkAPI;
}