//
//  Packing.h
//  FilePack
//
//  Created by chenqitan on 15-6-1.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//

#ifndef __FilePack__Packing__
#define __FilePack__Packing__

#include <vector>
#include <string>

class BuildPackage
{
public:
    BuildPackage();
    
private:
    std::string m_OutPutFile;
    std::vector<std::string> m_AbosoluteFileArray;
    std::vector<std::string> m_RelativeFileArray;
    std::vector<long> m_FileLength;
    
public:
    //void SetOutputFile(std::string OutputFileName);
    //void AddInputFile(std::string InputFileName);
    //要打包的绝对路径-基本路径-输出位置
    void BuildingPackage(std::vector<std::string> vec_path,std::string BasePath,std::string OutputPos);

private:
    static long GetFileSize(FILE *pf);
    
};


#endif /* defined(__FilePack__Packing__) */
