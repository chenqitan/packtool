//
//  FileManager.h
//  FilePack
//
//  Created by chenqitan on 15-6-3.
//  Copyright (c) 2015年 chenqitan. All rights reserved.
//

#ifndef __FilePack__FileManager__
#define __FilePack__FileManager__


#include <string>
#include <map>
#include <vector>
#include <pthread.h>
//文件-信息
struct FileInfo
{
    std::string myName;
    //预加载信息
    bool Loaded;
    int Reading;
    void* pMemory;
    //未预加载
    FILE * pFile;
    long FromAddr;
    long CurAddr;
    long AddrLength;
    long EndAddr;
    std::string CabName;
    FileInfo():
        myName(""),Loaded(0),Reading(0),pMemory(NULL),pFile(NULL),FromAddr(0),CurAddr(0),AddrLength(0),EndAddr(0),CabName(""){};
    
    FileInfo(std::string mn,bool ld,int rd,void *pm,FILE* pf,long fa,long ca,long al,long ea,std::string wcab):
        myName(mn),Loaded(ld),Reading(rd),pMemory(pm),pFile(pf),FromAddr(fa),CurAddr(ca),AddrLength(al),EndAddr(ea),CabName(wcab){};
    
    friend bool operator < (const struct FileInfo &ls, const struct FileInfo &rs)
    {
        return (ls.FromAddr < rs.FromAddr);
    }
    
};
//是否已经缓存
struct PreLoader
{
    bool State;
    void* Pnt;
    PreLoader():State(0),Pnt(NULL){};
    PreLoader(bool st,void* p):State(st),Pnt(p){};
    
};
//比较函数
struct CmpByTime
{
    bool operator()(const std::pair<std::string, int>& lhs, const std::pair<std::string, int>& rhs)
    {
        return lhs.second > rhs.second;
    }
};

//--API类--
class PackageInterface
{
public:PackageInterface();
        ~PackageInterface();
    
private:
    FileInfo FI;
    FILE *pCABA;
    
    std::vector<FILE*> Vec_CAB;
    std::vector<unsigned char*> Vec_Pnt;
    
    //Map-文件名==是否已经加载-内存位置-地址信息-包文件指针-包文件名
    std::map <std::string,FileInfo> m_FileMapInfo;
    //Map-文件名==调用次数
    std::map<std::string, int> m_FileMapTime;
    //Vector-文件名-调用次数
    std::vector<std::pair<std::string, int>> v_FileMapTime;
    //

    pthread_mutex_t Reading_Mutex; //互斥锁
    pthread_mutex_t Loading_Mutex; //互斥锁

    std::vector<std::string> PreLoad_Vector;
    //std::map<std::string,std::map <std::string,std::vector<unsigned long>>> m_CabMap;
    
private:
    void Header2Map(std::string BasePath,std::string CabFile);
    void PreLoad(std::string MostUsedFile);
    int MAX_PRLOAD;

public:
    //给主函数提供文件数组
    std::vector<std::string> m_FileArray;
    int m_Count;
    
public:
    void SetPreLoadNum(int max_load);
    //---------------------------
    void LoadCAB(std::string CabFile,std::string BasePath);
    bool HaveFile(std::string TargetFile);
    struct FileInfo* OpenFile(std::string TargetFile);
    void SeekFile(struct FileInfo* sfi,long offset,int fromwhere);
    void ReadFile(void* buffer,size_t size,size_t count,struct FileInfo *sfi);
    void CloseFile(struct FileInfo* sfi);
    //---------------------------
    void GetFileFromPack(std::string TargetFile,std::string SavePos);
    static std::string GetFileName(std::string PathFileName);
};
#endif /* defined(__FilePack__FileManager__) */
